package main

import (
	"flag"
	"path/filepath"
	"time"

	logs "github.com/Sirupsen/logrus"
	"github.com/golang/glog"
	informers "k8s.io/asb-controller/pkg/client/informers/externalversions"
	"k8s.io/asb-controller/pkg/signals"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	//"k8s.io/client-go/rest"

	examplecomclientset "k8s.io/asb-controller/pkg/client/clientset/versioned"
)

var (
	masterURL  string
	kubeconfig string
)

func kubeClient() (kubernetes.Interface, examplecomclientset.Interface) {
	stopCh := signals.SetupSignalHandler()

	//in-cluster
	// flag.StringVar(&kubeconfig, "kubeconfig", "", "Path to a kubeconfig. Only required if out-of-cluster.")
	// flag.StringVar(&masterURL, "master", "", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	// flag.Parse()

	// config, err := rest.InClusterConfig()
	// if err != nil {
	// 	panic(err.Error())
	// }

	//out-cluster
	home := homedir.HomeDir()
	flag.StringVar(&kubeconfig, "kubeconfig", filepath.Join(home, ".kube", "config"), "Path to a kubeconfig. Only required if out-of-cluster.")
	flag.StringVar(&masterURL, "master", "https://k8s-jermainea-master.northeurope.cloudapp.azure.com", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	flag.Parse()

	//use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags(masterURL, kubeconfig)
	if err != nil {
		glog.Fatalf("Error building kubeconfig: %s", err.Error())
	}

	// create the clientset
	asbClient, err := examplecomclientset.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	kubeClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(kubeClient, time.Second*30)
	asbInformerFactory := informers.NewSharedInformerFactory(asbClient, time.Second*30)

	controller := NewController(kubeClient, asbClient, kubeInformerFactory, asbInformerFactory)

	go kubeInformerFactory.Start(stopCh)
	go asbInformerFactory.Start(stopCh)

	if err = controller.Run(2, stopCh); err != nil {
		glog.Fatalf("Error running controller: %s", err.Error())
	}

	logs.Info("Successfully constructed k8s client")
	return kubeClient, asbClient
}

func main() {
	kubeClient()
}

#!/usr/bin/env bash
set -eo pipefail

declare ENVIR VAULT_ADDR VAULT_TOKEN AZ_SUBSCRIPTION_ID AZ_CLIENT_ID AZ_CLIENT_SECRET AZ_TENANT_ID

# This script will take the environment variables needed to run this controller
# from ARM_xxx and export them as AZ_xxx
# because the controller is using a library to deal with the env vars
# and no, I don't think we should deal with this bit witin the controller

usage() {
  echo "Usage: $0"
}

die() {
  echo "Missing environment variables. Please ensure all 6 env vars exist!"
}

checkenv() {
  if [[ -z "${ARM_SUBSCRIPTION_ID}" ]] \
  || [[ -z "${ARM_CLIENT_ID}" ]] \
  || [[ -z "${ARM_CLIENT_SECRET}" ]] \
  || [[ -z "${ARM_TENANT_ID}" ]] \
  || [[ -z "${VAULT_ADDR}" ]] \
  || [[ -z "${TOKEN}" ]]; \
  || [[ -z "${ENVIR}" ]]; then
    die
  fi
}

main() {
  checkenv
   export AZURE_SUBSCRIPTION_ID=${ARM_SUBSCRIPTION_ID} 
   export AZURE_CLIENT_ID=${ARM_CLIENT_ID} 
   export AZURE_CLIENT_SECRET=${ARM_CLIENT_SECRET} 
   export AZURE_TENANT_ID=${ARM_TENANT_ID}
   export VAULT_ADDRESS=${VAULT_ADDR}
   export VAULT_TOKEN=${TOKEN}
   export ENVIRONMENT=${ENVIR}
}

main "$@"

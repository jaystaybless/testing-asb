package main

import (
	"context"
	"fmt"
	"os"

	"github.com/Azure/azure-sdk-for-go/arm/keyvault"
	"github.com/Azure/azure-sdk-for-go/arm/resources/resources"
	"github.com/Azure/azure-sdk-for-go/arm/servicebus"
	"github.com/Azure/go-autorest/autorest/azure"
	"github.com/Azure/go-autorest/autorest/to"
	logs "github.com/Sirupsen/logrus"
	"github.com/golang/glog"
	"github.com/satori/uuid"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/azure-sdk-for-go-samples/helpers"
	"k8s.io/azure-sdk-for-go-samples/iam"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	asbv1 "k8s.io/asb-controller/pkg/apis/platform1.platform.mnscorp.net/v1"

	kvault "github.com/Azure/azure-sdk-for-go/services/keyvault/2016-10-01/keyvault"
)

var (
	resourceGroupName         string
	namespaceName             string
	subscriptionName          string
	topicName                 string
	location                  string
	authRuleName              string
	queueName                 string
	secretName                string
	primaryKey                string
	secondaryKey              string
	primaryConnectionString   string
	secondaryConnectionString string
	namespace                 string

	vaultName string
	ctx       = context.Background()
)

func init() {
	subscriptionID := getEnvVarOrExit("AZ_SUBSCRIPTION_ID")
	tenantID := getEnvVarOrExit("AZ_TENANT_ID")

	oauthConfig, err := azure.PublicCloud.OAuthConfigForTenant(tenantID)
	onErrorFail(err, "OAuthConfigForTenant failed")

	clientID := getEnvVarOrExit("AZ_CLIENT_ID")
	clientSecret := getEnvVarOrExit("AZ_CLIENT_SECRET")
	spToken, err := azure.NewServicePrincipalToken(*oauthConfig, clientID, clientSecret, azure.PublicCloud.ResourceManagerEndpoint)
	onErrorFail(err, "NewServicePrincipalToken failed")

	createClients(subscriptionID, spToken)
}

func CreateOrUpdateKeyvault(azureASB asbv1.AzureServiceBus) error {
	fmt.Printf("keyvaultCli() function has been called\n")
	applicationId := getEnvVarOrExit("AZ_CLIENT_ID")
	tenantId := uuid.FromStringOrNil(getEnvVarOrExit("AZ_TENANT_ID"))

	resourceGroupName := azureASB.Spec.ResourceGroupName
	location := azureASB.Spec.Location
	vaultName := azureASB.Spec.NamespaceName

	namespace := azureASB.Namespace
	environment := os.Getenv("ENVIRONMENT")
	tagName := "pltsrv-" + environment + "-" + location + "-" + namespace + "-ns-rg"
	tags := &map[string]*string{
		"tag-name": to.StringPtr(tagName),
	}

	properties := keyvault.VaultCreateOrUpdateParameters{
		Location: to.StringPtr(location),
		Tags:     tags,
		Properties: &keyvault.VaultProperties{
			Sku: &keyvault.Sku{
				Name:   keyvault.SkuName("Standard"),
				Family: to.StringPtr("A"),
			},
			AccessPolicies: &[]keyvault.AccessPolicyEntry{
				{
					TenantID: &tenantId,
					ObjectID: &applicationId,
					Permissions: &keyvault.Permissions{
						Secrets: &[]keyvault.SecretPermissions{
							keyvault.SecretPermissionsAll,
						},
					},
				},
			},
			TenantID: &tenantId,
		},
	}

	var err error
	fmt.Printf("Creating vault '%s'...\n", vaultName)
	_, err = keyvaultClient.CreateOrUpdate(resourceGroupName, vaultName, properties)
	if err != nil {
		fmt.Printf("An error has occured whilst creating vault... ERROR: '%s'...\n", err)
		return err
	}

	vault, err := keyvaultClient.Get(resourceGroupName, vaultName)
	if err != nil {
		return err
	}
	fmt.Printf("Vault info: '%s'...\n", vault)

	return nil
}

func deleteResourceGroup(azureASB asbv1.AzureServiceBus) error {
	fmt.Printf("TESTING TESTING 123 deleteResourceGroup")
	resourceGroupName := azureASB.Spec.ResourceGroupName

	fmt.Printf("Deleting resource group '%s'...\n", resourceGroupName)
	_, err := groupsClient.Delete(resourceGroupName, nil)
	onErrorFail(err, "Group delete failed")

	return nil
}

func createOrUpdateResourceGroup(azureASB asbv1.AzureServiceBus) error {
	resourceGroupName := azureASB.Spec.ResourceGroupName
	location := azureASB.Spec.Location
	tags := make(map[string]*string)
	if azureASB.Spec.Tags != nil {
		val := make([]string, len(azureASB.Spec.Tags))
		idx := 0
		for k, v := range azureASB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}

	if resourceGroupName == "" {
		runtime.HandleError(fmt.Errorf("%s: deployment name must be specified for Resource Group", resourceGroupName))
		return nil
	}
	if location == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Resource Group", location))
		return nil
	}

	// tags := &map[string]*string{
	// 	"tag-name": to.StringPtr(tagName),
	// }

	fmt.Printf("Creating resource group '%s'...\n", resourceGroupName)
	groupParams := resources.Group{
		Location: to.StringPtr(location),
		Tags:     &tags,
	}

	_, err := groupsClient.CreateOrUpdate(resourceGroupName, groupParams)
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Resource Group rule creation failed", err))
	}

	return nil
}

func createOrUpdateNamespace(azureASB asbv1.AzureServiceBus) error {
	resourceGroupName := azureASB.Spec.ResourceGroupName
	namespaceName := azureASB.Spec.NamespaceName
	location := azureASB.Spec.Location
	tags := make(map[string]*string)
	if azureASB.Spec.Tags != nil {
		val := make([]string, len(azureASB.Spec.Tags))
		idx := 0
		for k, v := range azureASB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}

	fmt.Printf("Creating namespace '%s'...\n", namespaceName)
	namespaceParams := servicebus.NamespaceCreateOrUpdateParameters{
		Location: to.StringPtr(location),
		Tags:     &tags,
		Sku: &servicebus.Sku{
			Tier: servicebus.SkuTierStandard,
			Name: servicebus.Standard,
		},
	}
	_, err := namespaceClient.CreateOrUpdate(resourceGroupName, namespaceName, namespaceParams, nil)
	if resourceGroupName == "" {
		runtime.HandleError(fmt.Errorf("%s: resourceGroupName name must be specified for Namespace ", resourceGroupName))
		return nil
	}
	if namespaceName == "" {
		runtime.HandleError(fmt.Errorf("%s: namespaceName name must be specified for Namespace ", namespaceName))
		return nil
	}
	if location == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", location))
		return nil
	}
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Namespace rule creation failed", err))
	}
	return nil
}

func deleteNamespace(azureASB asbv1.AzureServiceBus) error {
	fmt.Printf("deleteNamespace FUNCTION has been called...\n")

	resourceGroupName := azureASB.Spec.ResourceGroupName
	namespaceName := azureASB.Spec.NamespaceName
	location := azureASB.Spec.Location

	// tags := &map[string]*string{
	// 	"tag-name": to.StringPtr(tagName),
	// }

	tags := make(map[string]*string)
	if azureASB.Spec.Tags != nil {
		val := make([]string, len(azureASB.Spec.Tags))
		idx := 0
		for k, v := range azureASB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}

	fmt.Printf("DELETING namespace '%s'...\n", namespaceName)
	namespaceParams := servicebus.NamespaceCreateOrUpdateParameters{
		Location: to.StringPtr(location),
		Tags:     &tags,
		Sku: &servicebus.Sku{
			Tier: servicebus.SkuTierStandard,
			Name: servicebus.Standard,
		},
	}
	_, err := namespaceClient.CreateOrUpdate(resourceGroupName, namespaceName, namespaceParams, nil)
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Namespace DELETION failed", err))
	}
	return nil
}

func getKeysClient() kvault.BaseClient {
	keyClient := kvault.New()
	auth, _ := iam.GetKeyvaultToken(iam.AuthGrantType())
	keyClient.Authorizer = auth
	keyClient.AddToUserAgent(helpers.UserAgent())
	return keyClient
}

//get a clientset with in-cluster config.
func getClient() *kubernetes.Clientset {
	//config, err := rest.InClusterConfig()
	fmt.Printf("HELLO from getClient")
	config, err := clientcmd.BuildConfigFromFlags(masterURL, kubeconfig)
	if err != nil {
		glog.Fatal(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatal(err)
	}
	return clientset
}

func createOrUpdateAuthRule(azureASB asbv1.AzureServiceBus) error {
	resourceGroupName := azureASB.Spec.ResourceGroupName
	namespaceName := azureASB.Spec.NamespaceName
	authRuleName := azureASB.Spec.AuthRuleName
	location := azureASB.Spec.Location
	namespace := azureASB.Namespace
	environment := os.Getenv("ENVIRONMENT")
	tagName := "pltsrv-" + environment + "-" + location + "-" + namespace + "-ns-rg"

	if resourceGroupName == "" {
		runtime.HandleError(fmt.Errorf("%s: resourceGroupName name must be specified for Namespace ", resourceGroupName))
		return nil
	}
	if namespaceName == "" {
		runtime.HandleError(fmt.Errorf("%s: namespaceName name must be specified for Namespace ", namespaceName))
		return nil
	}
	if authRuleName == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", authRuleName))
		return nil
	}
	if location == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", location))
		return nil
	}

	fmt.Printf("Creating authorization rule '%s' for namespace '%s'...\n", authRuleName, namespaceName)
	ruleParams := servicebus.SharedAccessAuthorizationRuleCreateOrUpdateParameters{
		SharedAccessAuthorizationRuleProperties: &servicebus.SharedAccessAuthorizationRuleProperties{
			Rights: &[]servicebus.AccessRights{
				servicebus.Listen,
				servicebus.Manage,
				servicebus.Send,
			},
		},
	}

	_, err := namespaceClient.CreateOrUpdateAuthorizationRule(resourceGroupName, namespaceName, authRuleName, ruleParams)
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Authorization rule creation failed", err))
		//onErrorFail(err, "Authorization rule create failed")
	} else {
		fmt.Printf("Listing keys for '%s' namespace...\n", namespaceName)
		// add some logic to handle missing keys in Azure
		keys, err := namespaceClient.ListKeys(resourceGroupName, namespaceName, authRuleName)
		if err != nil {
			fmt.Sprintf("\tKey name: %s\n", err) //"List keys failed %v"
		} else {
			secretName := namespaceName //secretName := *keys.KeyName
			primaryKey := *keys.PrimaryKey
			secondaryKey := *keys.SecondaryKey
			primaryConnectionString := *keys.PrimaryConnectionString
			secondaryConnectionString := *keys.SecondaryConnectionString

			//NOTE: The code below will create secrets in Hashicorp Vault
			// vault := vaultCli()

			// keyName := "PaaS/kubeconfig/clusters/" + namespaceName

			// keyData := map[string]interface{}{
			// 	"PrimaryKey":                primaryKey,
			// 	"SecondaryKey":              secondaryKey,
			// 	"PrimaryConnetionString":    primaryConnectionString,
			// 	"SecondaryConnectionString": secondaryConnectionString,
			// }

			// //create secrets in hashicorp vault
			// secretValues, err := vault.Logical().Write(keyName, keyData)
			// if err != nil {
			// 	fmt.Printf("Cannot create secrets in vault %s\n", err)
			// }
			// fmt.Printf("secretValues: %v\n", secretValues)

			//create secrets in kubernetes cluster
			kubeClient := getClient()

			secretData := map[string]string{
				"PrimaryKey":                primaryKey,
				"SecondaryKey":              secondaryKey,
				"PrimaryConnetionString":    primaryConnectionString,
				"SecondaryConnectionString": secondaryConnectionString,
			}

			sc := &v1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      secretName, //Name: namespaceName
					Namespace: namespace,
					Labels: map[string]string{
						"name": tagName,
					},
				},
				StringData: secretData,
			}

			_, err = kubeClient.CoreV1().Secrets(namespace).Get(secretName, metav1.GetOptions{})
			if errors.IsNotFound(err) {
				fmt.Printf("Secret %s in namespace %s not found\n", secretName, namespace)
				_, err = kubeClient.CoreV1().Secrets(namespace).Create(sc)
				if err != nil {
					//fmt.Printf("Cannot update create %s\n", sc)
					glog.Fatalf("Cannot create secret %s", sc)
				}
			}
			if statusError, isStatus := err.(*errors.StatusError); isStatus {
				fmt.Printf("Error getting secret %s in namespace %s: %v\n",
					secretName, namespace, statusError.ErrStatus.Message)
			}
			if err != nil {
				panic(err.Error())
			} else {
				fmt.Printf("Found secret %s in namespace %s\n", secretName, namespace)
				fmt.Printf("Secret %s in namespace %s already exists\n", secretName, namespace)
				fmt.Printf("UPDATING Secret %s in namespace %s\n", sc.Name, namespace)
				_, err = kubeClient.CoreV1().Secrets(namespace).Update(sc)
				if err != nil {
					glog.Fatalf("Cannot update secret %s", sc)
				}
			}

			//Creating Secrets In KeyVault
			fmt.Printf("------Creating Keys in KeyVault...------\n")
			//vaultName := azureASB.Spec.NamespaceName
			vaultName := "jermainea-test-logging"
			//kvault = kvault.KeyBundle()
			//vaultsClient := getVaultsClient()
			vault, err := keyvaultClient.Get(resourceGroupName, vaultName)
			if err != nil {
				return err //fmt.Printf(err)

			}

			vaultURL := *vault.Properties.VaultURI
			fmt.Printf("Vault URL:  '%s'...\n", vaultURL)

			keyClient := getKeysClient()

			keyParams := kvault.KeyCreateParameters{
				KeyAttributes: &kvault.KeyAttributes{
					Enabled: to.BoolPtr(true),
				},
				KeySize: to.Int32Ptr(2048), // As of writing this sample, 2048 is the only supported KeySize.
				KeyOps: &[]kvault.JSONWebKeyOperation{
					kvault.Encrypt,
					kvault.Decrypt,
				},
				Kty: kvault.RSA,
			}

			keyName := primaryConnectionString
			//secondaryConnectionString := *keys.SecondaryConnectionString

			_, err = keyClient.CreateKey(ctx, vaultURL, keyName, keyParams)
			fmt.Printf("vault key info:'%s'...\n", vault)
			fmt.Printf("vault keyName info:'%s'...\n", keyName)

			if err != nil {
				fmt.Errorf("Vault Key creation failed: %v", err)
			}

		}
	}
	return nil
}

func createOrUpdateQueue(azureASB asbv1.AzureServiceBus) error {
	resourceGroupName := azureASB.Spec.ResourceGroupName
	namespaceName := azureASB.Spec.NamespaceName
	queueName := azureASB.Spec.QueueName
	location := azureASB.Spec.Location

	if resourceGroupName == "" {
		runtime.HandleError(fmt.Errorf("%s: resourceGroupName name must be specified for Namespace ", resourceGroupName))
		return nil
	}
	if namespaceName == "" {
		runtime.HandleError(fmt.Errorf("%s: namespaceName name must be specified for Namespace ", namespaceName))
		return nil
	}
	if queueName == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", queueName))
		return nil
	}
	if location == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", location))
		return nil
	}

	fmt.Printf("Creating queue '%s'...\n", queueName)
	queueParams := servicebus.QueueCreateOrUpdateParameters{
		Location: to.StringPtr(location),
		QueueProperties: &servicebus.QueueProperties{
			EnablePartitioning: to.BoolPtr(true),
		},
	}
	_, err := queuesClient.CreateOrUpdate(resourceGroupName, namespaceName, queueName, queueParams)
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Queue rule creation failed", err))
	}
	return nil
}

func createOrUpdateTopic(azureASB asbv1.AzureServiceBus) error {
	resourceGroupName := azureASB.Spec.ResourceGroupName
	namespaceName := azureASB.Spec.NamespaceName
	topicName := azureASB.Spec.TopicName
	location := azureASB.Spec.Location

	if resourceGroupName == "" {
		runtime.HandleError(fmt.Errorf("%s: resourceGroupName name must be specified for Namespace ", resourceGroupName))
		return nil
	}
	if namespaceName == "" {
		runtime.HandleError(fmt.Errorf("%s: namespaceName name must be specified for Namespace ", namespaceName))
		return nil
	}
	if topicName == "" {
		runtime.HandleError(fmt.Errorf("%s: topic name name must be specified for Namespace ", topicName))
		return nil
	}
	if location == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", location))
		return nil
	}

	fmt.Printf("Creating topic '%s'...\n", topicName)
	topicsParams := servicebus.TopicCreateOrUpdateParameters{
		Location: to.StringPtr(location),
		TopicProperties: &servicebus.TopicProperties{
			EnablePartitioning: to.BoolPtr(true),
		},
	}
	_, err := topicsClient.CreateOrUpdate(resourceGroupName, namespaceName, topicName, topicsParams)
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Topic rule creation failed", err))
	}

	return nil
}

func createOrUpdateSubscription(azureASB asbv1.AzureServiceBus) error {
	resourceGroupName := azureASB.Spec.ResourceGroupName
	namespaceName := azureASB.Spec.NamespaceName
	subscriptionName := azureASB.Spec.SubscriptionName
	topicName := azureASB.Spec.TopicName
	location := azureASB.Spec.Location

	if resourceGroupName == "" {
		runtime.HandleError(fmt.Errorf("%s: resourceGroupName name must be specified for Namespace ", resourceGroupName))
		return nil
	}
	if namespaceName == "" {
		runtime.HandleError(fmt.Errorf("%s: namespaceName name must be specified for Namespace ", namespaceName))
		return nil
	}
	if subscriptionName == "" {
		runtime.HandleError(fmt.Errorf("%s: subscription name must be specified for Namespace ", subscriptionName))
		return nil
	}
	if location == "" {
		runtime.HandleError(fmt.Errorf("%s: location name must be specified for Namespace ", location))
		return nil
	}
	fmt.Printf("Creating subscription '%s'...\n", subscriptionName)
	subParams := servicebus.SubscriptionCreateOrUpdateParameters{
		Location: to.StringPtr(location),
	}
	_, err := subClient.CreateOrUpdate(resourceGroupName, namespaceName, topicName, subscriptionName, subParams)
	if err != nil {
		runtime.HandleError(fmt.Errorf("%s: Subscription rule creation failed", err))
	}

	return nil
}

func listResource(key string) (err error) {
	namespace, name, err := cache.SplitMetaNamespaceKey(key)

	logs.Info(fmt.Sprintf("%v asb is currently in the %v namespace", name, namespace))

	return err
}

func createClients(subID string, token *azure.ServicePrincipalToken) {
	// vaultURL := fmt.Sprintf("https://jermaine-crd-test.vault.azure.net")

	groupsClient = resources.NewGroupsClient(subID)
	groupsClient.Authorizer = token

	namespaceClient = servicebus.NewNamespacesClient(subID)
	namespaceClient.Authorizer = token

	queuesClient = servicebus.NewQueuesClient(subID)
	queuesClient.Authorizer = token

	topicsClient = servicebus.NewTopicsClient(subID)
	topicsClient.Authorizer = token

	subClient = servicebus.NewSubscriptionsClient(subID)
	subClient.Authorizer = token

	keyvaultClient = keyvault.NewVaultsClient(subID)
	keyvaultClient.Authorizer = token
}

// getEnvVarOrExit returns the value of specified environment variable or terminates if it's not defined.
func getEnvVarOrExit(varName string) string {
	value := os.Getenv(varName)
	if value == "" {
		fmt.Printf("Missing environment variable %s\n", varName)
		os.Exit(1)
	}

	return value
}

// onErrorFail prints a failure message and exits the program if err is not nil.
func onErrorFail(err error, message string) {
	if err != nil {
		fmt.Printf("%s: %s\n", message, err)
		os.Exit(1)
	}
}

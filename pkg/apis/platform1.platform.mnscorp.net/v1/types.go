package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AzureServiceBus describes a AzureServiceBus.
type AzureServiceBus struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec AzureServiceBusSpec `json:"spec"`
}

// AzureServiceBusSpec is the spec for a Foo resource
type AzureServiceBusSpec struct {
	Location          string            `json:"location"`
	ResourceGroupName string            `json:"resourceGroupName"`
	NamespaceName     string            `json:"namespaceName"`
	AuthRuleName      string            `json:"authRuleName"`
	QueueName         string            `json:"queueName"`
	TopicName         string            `json:"topicName"`
	SubscriptionName  string            `json:"subscriptionName"`
	Tags              map[string]string `json:"tags,omitempty"`
	//Encoding string `json:"encoding,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AzureServiceBusList is a list of AzureServiceBus resources
type AzureServiceBusList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []AzureServiceBus `json:"items"`
}

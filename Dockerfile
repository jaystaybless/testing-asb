FROM golang:1.9 as gobuild

ADD . /go/src/k8s.io/asb-controller

WORKDIR /go/src/k8s.io/asb-controller/

RUN make controller-static

FROM alpine

RUN apk add --update ca-certificates && \
      rm -rf /var/cache/apk/* /tmp/*

RUN update-ca-certificates

COPY --from=gobuild /go/src/k8s.io/asb-controller/controller-static /asb-controller

# ENV AZURE_TENANT_ID=
# ENV AZURE_CLIENT_ID=
# ENV AZURE_CLIENT_SECRET=
# ENV AZURE_SUBSCRIPTION_ID=
# ENV ENVIRONMENT=
# ENV VAULT_ADDRESS=
# ENV VAULT_TOKEN=

CMD ["/asb-controller"]

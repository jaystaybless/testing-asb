GO = go
GOFMT = gofmt
GOLINT = golint

GO_PACKAGES = ./cmd/... ./pkg/...

all: controller

controller:
	$(GO) build -o $@ ./cmd/controller

controller-static:
	CGO_ENABLED=0 $(GO) build -installsuffix cgo -o $@ .